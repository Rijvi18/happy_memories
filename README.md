![image](/uploads/68e4f60a81a0dc5d726c67c8cf23d51b/image.png)

Using React, Node.js, Express & MongoDB you'll learn how to build a Full Stack MERN Application - from start to finish. The App is called "Happy_Memories" and it is a simple social media app that allows users to post interesting events that happened in their lives.

By the end of this video, you will have a strong understanding of how the MERN Stack works.

Setup:

run npm i && npm start for both client and server side to start the app
